from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

from .forms import TaskForm
from .models import Task


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form = form.save()  # not 100% sure I understand this
            return redirect("home")  # may need to change this
    else:
        form = TaskForm()
    context = {"form": form}
    return render(request, "tasks/create.html", context)


def list_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": tasks,
    }
    return render(request, "tasks/list.html", context)
